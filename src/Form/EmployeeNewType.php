<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeNewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, ['label' => false, 'attr' => array('placeholder' => 'First Name (required)', 'class' => 'form-control')])
            ->add('lastname', TextType::class, ['label' => false, 'attr' => array('placeholder' => 'Last Name (required)', 'class' => 'form-control')])
            ->add('department', TextType::class, ['label' => false, 'attr' => array('placeholder' => 'Department (required)', 'class' => 'form-control')])
            ->add('profile', TextareaType::class, ['label' => false, 'attr' => array('placeholder' => 'Profile', 'class' => 'form-control')])
//            ->add('test', EntityType::class, [
//                'placeholder' => 'Select one option',
//                'label' => 'Payment is for',
//                'class' => Test::class,
//                'choice_label' => function ($test) {
//                    return $test->getName() .'  ($'. $test->getCost() . ')';
//                },
//                'query_builder' => function(TestRepository $repo) {
//                    return $repo->createAlphabeticalQueryBuilder();
//                }
//            ])
            ->add('save', SubmitType::class, array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-primary mt-3')
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Employee::class,
        ));
    }
}
