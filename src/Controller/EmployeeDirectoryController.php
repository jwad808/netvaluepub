<?php
namespace App\Controller;
use App\Repository\EmployeeRepository;
use App\Entity\Employee;
use App\Form\EmployeeNewType;
use App\Form\EmployeeEditType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Knp\Component\Pager\PaginatorInterface;

class EmployeeDirectoryController extends AbstractController {

    /**
     * @Route("/", methods={"GET","POST"})
     */
    public function indexAction(EmployeeRepository $repository, Request $request, PaginatorInterface $paginator) {
        $form = $this->buildForm();
        if($request->getRealMethod() == "GET")
        {
            $q = $request->query->get('q');
            $query = $repository->getWithSearchQueryBuilder($q);
        }
        else
        {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $dept = $form->getData();
                $list = $repository->getWithSearchDirQueryBuilder($dept["department"]->getDepartment());
                $query = $list->getQuery();
            }
            else
            {
                return $this->render('employees/index.html.twig', [
//                'pagination' => [],
                    'form' => $form->createView()
                ]);
            }
        }
        dump($query);
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );
        return $this->render('employees/index.html.twig', [
            'form' => $form->createView(),
            'pagination' => $pagination
        ]);

    }
    /**
     * @Route("/employee/new", name="new_employee", methods={"GET","POST"})
     */
    public function newAction(Request $request) {

        $employee = new Employee();
        $form = $this->createForm(EmployeeNewType::class, $employee);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $employee = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            try {
                $entityManager->persist($employee);
                $entityManager->flush();
            } catch (DBALException $e) {
                return $this->render('default/failure.html.twig', array(
                    'errMesg' => $e->getMessage()
                ));
            }

            return $this->redirectToRoute('app_employeedirectory_index');
        }
        return $this->render('employees/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/employee/edit/{id}", name="edit_employee", methods={"GET","POST"})
     */
    public function editAction(Request $request, $id) {
        //$employee = new Employee();
        $employee = $this->getDoctrine()->getRepository(Employee::class)->find($id);
        $form = $this->createForm(EmployeeEditType::class, $employee);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('app_employeedirectory_index');
        }
        return $this->render('employees/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/employee/{id}", name="employee_show")
     */
    public function showAction($id) {
        $employee = $this->getDoctrine()->getRepository(Employee::class)->find($id);
        return $this->render('employees/show.html.twig', array('employee' => $employee));
    }
    /**
     * @Route("/employee/delete/{id}", name="employee_delete")
     */
    public function deleteAction(Request $request, $id) {
//        $employee = $this->getDoctrine()->getRepository(Employee::class)->find($id);
//        $entityManager = $this->getDoctrine()->getManager();
//        $entityManager->remove($employee);
//        $entityManager->flush();

        return $this->redirectToRoute('app_employeedirectory_index');
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function buildForm()
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('app_employeedirectory_index'))
            ->setMethod('POST')
            ->add('department', EntityType::class, [
                'placeholder' => 'Select department',
                'class' => Employee::class,
                'choice_label' => function(Employee $employee) {
                    return $employee->getDepartment();
                },
                'query_builder' => function(EmployeeRepository $repository) {
                    return $repository->createAlphabeticalQueryBuilder();
                }
            ])
            ->getForm();
        return $form;
    }

}