<?php

namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Employee::class);
    }
    /**
     * @param string|null $term
     */
    public function getWithSearchQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('e');
//            ->innerJoin('e.article', 'a')
//            ->addSelect('e');
        if ($term) {
            $qb->andWhere('e.firstname LIKE :term OR e.lastname LIKE :term OR e.department LIKE :term OR e.profile LIKE :term')
                ->setParameter('term', '%' . $term . '%')
            ;
        }
        return $qb
//            ->orderBy('c.createdAt', 'DESC')
            ->orderBy('e.lastname','DESC')
            ;
    }

    /**
     * @param null|string $term
     * @return QueryBuilder
     */
    public function getWithSearchDirQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('e');
        if ($term) {
            $qb->where('e.department = :term')
                ->setParameter('term', $term)
            ;
        }
        return $qb
            ->orderBy('e.department','DESC')
            ;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('e')
            ->groupBy('e.department')
            ->orderBy('e.department', 'ASC');
    }
    // /**
    //  * @return Employee[] Returns an array of Employee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Employee
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
