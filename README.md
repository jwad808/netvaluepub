# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*Dev Test for NetValue, basic Symfony 4 CRUD application with emplyee directory search & pagination
*v1.0

### How do I get set up? ###

## Install dependencies
composer install

### Edit the env file and add DB params
Please enter your mysql DB username and password to .env file in root

### Create Employee schema
php bin/console doctrine:migrations:diff

### Run migrations
php bin/console doctrine:migrations:migrate

## Database configuration
mariadb  Ver 15.1 Distrib 10.4.6-MariaDB, for osx10.12 (x86_64) 

### Author
Todd Jackson



